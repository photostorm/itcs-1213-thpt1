/**
 * This class holds and convert currency amounts into dollars, pounds, euros and yen. 
 * 
 * @author Justin E. Ervin
 * @version 1/26/2014
 */

public class CurrencyConverter {
	// Declares all constants variable
	private final double EUROS_RATE = 0.7311; // Stores the rate of currency of euros compare to dollars
	private final double YEN_RATE = 102.32; // Stores the rate of currency of yen compare to dollars
	private final double GBP_RATE = 0.6064; // Stores the rate of currency of pounds compare to dollars

	// Declares all needed variable
	private double euro; // Stores the monetary amount in euros
	private double yen; // Stores the monetary amount in yen
	private double gbp; // Stores the monetary amount in pounds
	private double dollar; // Stores the monetary amount in dollars

	/**
	 * Constructor for objects of class CurrencyConverter
	 * 
	 * @param the monetary amount in dollars, euros, pounds or yen
	 * @param the type of currency
	 */
	public CurrencyConverter(double amount, char currencyType) {
		if (currencyType == 'd') {
			// Calculate amount for each currency from the monetary amount of dollars entered
			this.dollar = amount;
			this.euro = amount * EUROS_RATE;
			this.yen = amount * YEN_RATE;
			this.gbp = amount * GBP_RATE;
		} else if (currencyType == 'e') {
			// Calculate amount for each currency from the monetary amount of euros entered
			this.dollar = amount / EUROS_RATE;
			this.euro = amount;
			this.yen = (amount / EUROS_RATE) * YEN_RATE;
			this.gbp = (amount / EUROS_RATE) * GBP_RATE;
		} else if (currencyType == 'g') {
			// Calculate amount for each currency from the monetary amount of pounds entered
			this.dollar = (amount / GBP_RATE);
			this.euro = (amount / GBP_RATE) * EUROS_RATE;
			this.yen = (amount / GBP_RATE) * YEN_RATE;
			this.gbp = amount;
		} else if (currencyType == 'y') {
			// Calculate amount for each currency from the monetary amount of yen entered
			this.dollar = (amount / YEN_RATE);
			this.euro = (amount / YEN_RATE) * EUROS_RATE;
			this.yen = amount;
			this.gbp = (amount / YEN_RATE) * GBP_RATE;
		} else {
			// Set all currency amounts to zero if an invalid currency type was entered
			this.dollar = 0;
			this.euro = 0;
			this.yen = 0;
			this.gbp = 0;
		}
	}

	/**
	 * This method allows the user to get the monetary amount in dollars, euros, pounds or yen 
	 * d = dollar, e = euro, g = great british pounds, y = yen
	 * 
	 * @param type of currency
	 * @return the monetary amount in dollars, euros, pounds or yen
	 */
	public double getCurrency(char currencyType) {
		if (currencyType == 'd') {
			// Returns the monetary amount of currency in dollars
			return dollar;
		} else if (currencyType == 'e') {
			// Returns the monetary amount of currency in euros
			return euro;
		} else if (currencyType == 'g') {
			// Returns the monetary amount of currency in pounds
			return gbp;
		} else if (currencyType == 'y') {
			// Returns the monetary amount of currency in yen
			return yen;
		} else {
			// Returns zero if an invalid currency type was entered
			return 0;
		}
	}

	/**
	 * This method allows the user to change the monetary amount in dollar and maintains the
	 * consistency of the all currency scales. 
	 * d = dollar, e = euro, g = great british pounds, y = yen
	 * 
	 * @param the monetary amount in dollars, euros, pounds or yen
	 * @param type of currency
	 */
	public void setCurrency(double amount, char currencyType) {
		if (currencyType == 'd') {
			// Calculate amount for each currency from the monetary amount of dollars entered
			this.dollar = amount;
			this.euro = amount * EUROS_RATE;
			this.yen = amount * YEN_RATE;
			this.gbp = amount * GBP_RATE;
		} else if (currencyType == 'e') {
			// Calculate amount for each currency from the monetary amount of euros entered
			this.dollar = amount / EUROS_RATE;
			this.euro = amount;
			this.yen = (amount / EUROS_RATE) * YEN_RATE;
			this.gbp = (amount / EUROS_RATE) * GBP_RATE;
		} else if (currencyType == 'g') {
			// Calculate amount for each currency from the monetary amount of pounds entered
			this.dollar = (amount / GBP_RATE);
			this.euro = (amount / GBP_RATE) * EUROS_RATE;
			this.yen = (amount / GBP_RATE) * YEN_RATE;
			this.gbp = amount;
		} else if (currencyType == 'y') {
			// Calculate amount for each currency from the monetary amount of yen entered
			this.dollar = (amount / YEN_RATE);
			this.euro = (amount / YEN_RATE) * EUROS_RATE;
			this.yen = amount;
			this.gbp = (amount / YEN_RATE) * GBP_RATE;
		}
	}
}
