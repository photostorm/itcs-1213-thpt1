/**
 * This class was written to test the CurrencyConverter class.
 * 
 * @author Justin E. Ervin
 * @version 1/26/2014
 */

public class Driver {

	/**
	 * Execution of this program starts in the main( ) method
	 * 
	 * @param Java arguments
	 */
	public static void main(String[] args) {
		// Create a reference variable
		CurrencyConverter currency;

		// Create an instance using the CurrencyConverter class with monetary amount being 100 dollars
		currency = new CurrencyConverter(100, 'd');

		// Print the values of the fields of currency object
		System.out.println(currency.getCurrency('d') + " dollars = " + currency.getCurrency('e') + " euros = " + currency.getCurrency('g') + " pounds = " + currency.getCurrency('y') + " yen");

		// Change the monetary amount to 100 euros
		currency.setCurrency(100, 'e');

		// Print the values of the fields of currency object
		System.out.println(currency.getCurrency('d') + " dollars = " + currency.getCurrency('e') + " euros = " + currency.getCurrency('g') + " pounds = " + currency.getCurrency('y') + " yen");

		// Change the monetary amount to 100 pounds
		currency.setCurrency(100, 'g');

		// Print the values of the fields of currency object
		System.out.println(currency.getCurrency('d') + " dollars = " + currency.getCurrency('e') + " euros = " + currency.getCurrency('g') + " pounds = " + currency.getCurrency('y') + " yen");

		// Change the monetary amount to 200 yen
		currency.setCurrency(200, 'y');

		// Print the values of the fields of currency object
		System.out.println(currency.getCurrency('d') + " dollars = " + currency.getCurrency('e') + " euros = " + currency.getCurrency('g') + " pounds = " + currency.getCurrency('y') + " yen");

		// Change the monetary amount to 200 dollars
		currency.setCurrency(200, 'd');

		// Print the values of the fields of currency object
		System.out.println(currency.getCurrency('d') + " dollars = " + currency.getCurrency('e') + " euros = " + currency.getCurrency('g') + " pounds = " + currency.getCurrency('y') + " yen");
	}

}
